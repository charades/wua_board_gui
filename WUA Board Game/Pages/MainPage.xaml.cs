﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WUA_Board_Game.Board_Game;
using WUA_Board_Game.Board_Game_Interfaces;
using WUA_Board_Game.Pusher_Game;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WUA_Board_Game.Pages
{
    /// <summary>
    /// Main Board GUI
    /// </summary>
    public sealed partial class MainPage : Page, IObserver<IMove>
    {

        private readonly IBoard _board = new PusherBoard();
        public int DragStartRow { get; set; }
        public int DragStartCol { get; set; }

        public Viewbox DragStartViewbox { get; set; }

        private IDisposable boardUnsubscriber;

        private static int GAME_BOARD_LENGTH = 8;
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void UIElement_OnDragStarting(UIElement sender, DragStartingEventArgs args)
        {
            DragStartRow = (Grid.GetRow(sender as Viewbox));
            DragStartCol = (Grid.GetColumn(sender as Viewbox));

            DragStartViewbox = sender as Viewbox;
        }

        private void SquareDragOver(object sender, DragEventArgs e)
        {
            if (_board.IsLegalMove(DragStartRow, DragStartCol, Grid.GetRow(sender as FrameworkElement),
                Grid.GetColumn(sender as FrameworkElement)))
            {
                e.AcceptedOperation = Windows.ApplicationModel.DataTransfer.DataPackageOperation.Move;
                e.DragUIOverride.IsCaptionVisible = false;
                e.DragUIOverride.IsGlyphVisible = false;
            }
            else
            {
                e.AcceptedOperation = Windows.ApplicationModel.DataTransfer.DataPackageOperation.None;
                e.DragUIOverride.IsCaptionVisible = true;
                e.DragUIOverride.IsGlyphVisible = true;
            }
            
        }

        private void SquareDrop(object sender, DragEventArgs e)
        {
            var dropRow = Grid.GetRow(sender as FrameworkElement);
            var dropColumn = Grid.GetColumn(sender as FrameworkElement);

            if (dropRow == DragStartRow && dropColumn == DragStartCol)
                return;


            _board.MovePiece(DragStartRow, DragStartCol, dropRow, dropColumn);

        }

        private void NewGameButtonClicked(object sender, RoutedEventArgs e)
        {
            var player1Type = GetPlayerType(p1ComboBox.SelectedValue?.ToString());
            var player2Type = GetPlayerType(p2ComboBox.SelectedValue?.ToString());

            _board.StartNewGame(player1Type, player2Type);
            boardUnsubscriber = _board.Subscribe(this);

            FillGridWithPieces();

            /*
            if (p1ComboBox.SelectedValue?.ToString() == "Computer" || p2ComboBox.SelectedValue?.ToString() == "Computer")
            {
                var socketListener = new AsyncServerSocketListener();
                socketListener.Connect();
            }
            */
        }

        private PlayerType GetPlayerType(string selectedPlayerType)
        {
            switch (selectedPlayerType)
            {
                case "Human":
                    return PlayerType.Human;
                    
                case "Computer":
                    return PlayerType.Computer;

                default:
                    return PlayerType.Human;
       
            }
        }

        /// <summary>
        /// Fills the UI grid with the pieces that are on the current game Board.
        /// </summary>
        private void FillGridWithPieces()
        {
            for (var i = 0; i < GAME_BOARD_LENGTH; i++)
            {
                FillRowWithPieces(i);
            }
        }

        private void FillRowWithPieces(int row)
        {
            var itemsInRow = boardGrid.Children
                 .Cast<FrameworkElement>()
                 .Where(i => Grid.GetRow(i) == row);

            var col = 0;
            foreach (var viewbox in itemsInRow.OfType<Viewbox>())
            {
                var piece = _board.Pieces[row, col];
                if (piece == null)
                {
                    //Clean the board
                    viewbox.Child = null;
                    continue;
                }

                AddTextToLog("Creating " + ((TeamFlag) piece.Owner.Team) + " " + piece.GetType().Name 
                    + " at: " + (Columns) col + row + '\n');

                viewbox.Child = _board.Pieces[row, col].Image;

                col++;
            }
        }

        /// <summary>
        /// Will transfer the specified Viewbox Child to the target Viewbox Child.
        /// The target Child is overwritten.
        /// </summary>
        /// <param name="source">The Viewbox from which to take the Child</param>
        /// <param name="target">The Viewbox to which to make the source Child. The target Child is overwritten.</param>
        private void MoveViewboxChild(Viewbox source, Viewbox target)
        {
            var childTemp = source.Child;

            //A child can only be the child of 1 parent at a time, so remove it from source first.
            source.Child = null;

            target.Child = childTemp;
        }

        private Viewbox GetViewbox(int row, int column)
        {
            return boardGrid.Children.OfType<Viewbox>().FirstOrDefault(viewbox => Grid.GetRow(viewbox) == row && Grid.GetColumn(viewbox) == column);
        }

        /// <summary>
        /// Logs the move in the main window log panel.
        /// </summary>
        /// <param name="dropRow"></param>
        /// <param name="dropCol"></param>
        private void LogMove(int dropRow, int dropCol)
        {
            var move = "";

            //Source
            move += ((Columns)DragStartCol).ToString();
            move += DragStartRow + 1;

            move += ":";

            move += ((Columns)dropCol).ToString();
            move += dropRow + 1;

            AddTextToLog("Moved " + move + "\n");
        }

        private void LogMove(IMove move)
        {
            var moveText = "";

            moveText += move.Team;
            moveText += " ";
            moveText += move.MovedPiece?.GetType().Name;
            moveText += " ";
            moveText += (Columns) move.Origin.Column + move.Origin.Row.ToString();
            moveText += ":";
            moveText += (Columns) move.Target.Column + move.Target.Row.ToString();

            AddTextToLog(moveText + "\n");
        }

        /// <summary>
        /// Will add text to the game log and scroll down automatically.
        /// </summary>
        /// <param name="text">The text to add to the game log.</param>
        private void AddTextToLog(string text)
        {
            log.Text += text;
            logScrollView.ChangeView(null, logScrollView.ScrollableHeight, null);
        }

        private void EndGame(IMove move)
        {
            ShowEndGameDialog(move);
        }

        private void ShowEndGameDialog(IMove move)
        {
            var dialog = new ContentDialog()
            {
                Title = "Game has ended",
                MaxWidth = this.ActualWidth
            };

            dialog.Content = (TeamFlag) move.Team + " has won!";
            dialog.PrimaryButtonText = "OK";
            dialog.ShowAsync();
        }

        void IObserver<IMove>.OnCompleted()
        {
            throw new NotImplementedException();
        }

        void IObserver<IMove>.OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        void IObserver<IMove>.OnNext(IMove value)
        {
            MoveViewboxChild(
                GetViewbox(value.Origin.Row, value.Origin.Column),
                GetViewbox(value.Target.Row, value.Target.Column)
                );

            LogMove(value);

            if (value.Victory)
                EndGame(value);
        }
    }
}
