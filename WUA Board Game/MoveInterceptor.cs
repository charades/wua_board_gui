﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WUA_Board_Game
{
    /// <summary>
    /// This singleton should intercept every move that is to be played on the board.
    /// </summary>
    class MoveInterceptor
    {
        private static MoveInterceptor instance;
        private String move;

        private MoveInterceptor() { }

        public static MoveInterceptor Instance
        {
            get
            {
                if (instance == null)
                    instance = new MoveInterceptor();

                return instance;
            }
        }

        public void intercept(String move)
        {
            this.move = move;
        }
      
    }
}
