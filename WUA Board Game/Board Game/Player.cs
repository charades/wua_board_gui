﻿using Windows.UI.Notifications;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Board_Game
{
    internal class Player : IPlayer
    {
        public TeamFlag Team { get; set; }
        public PlayerType PlayerType { get; set; }

        public Player(TeamFlag team, PlayerType playerType)
        {
            this.Team = team;
            this.PlayerType = playerType;
        }

        public int GetDirection()
        {
            return Team == TeamFlag.White ? 1 : -1;
        }

        public override bool Equals(object obj)
        {
            var player = obj as Player;

            return this.Team == player?.Team;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
