﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Board_Game
{
    internal class BoardState : IBoardState
    {
        public IPiece[,] Pieces { get; }

        public BoardState(IPiece[,] pieces)
        {
            Pieces = new IPiece[Board.BOARD_SIZE, Board.BOARD_SIZE];
            Array.Copy(pieces, Pieces, Board.BOARD_SIZE*Board.BOARD_SIZE);
        }
    }
}
