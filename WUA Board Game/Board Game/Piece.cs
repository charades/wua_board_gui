﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using WUA_Board_Game.Board_Game_Interfaces;
using WUA_Board_Game.Pusher_Game;

namespace WUA_Board_Game.Board_Game
{
    internal abstract class Piece : IPiece
    {
        public List<IMove> LegalMoves { get; set; }
        public IPlayer Owner { get; set; }
        public Image Image { get; set; }
        public abstract int Score { get; }

        protected void InitImage(Uri whiteUri, Uri blackUri)
        {
            Image = new Image();
            var pawnBitmap = new BitmapImage();

            switch ((TeamFlag) Owner.Team)
            {
                
                case TeamFlag.White:
                default:
                    pawnBitmap.UriSource = whiteUri;
                    break;

                case TeamFlag.Black:
                    pawnBitmap.UriSource = blackUri;
                    break;

            }

            Image.Source = pawnBitmap;
        }
        public bool Move(int row, int col)
        {
            throw new NotImplementedException();
        }

        public bool IsLegalMove(int tRow, int tCol)
        {
            return LegalMoves.Any(move => move.Target.Row == tRow && move.Target.Column == tCol);
        }

        public abstract void ComputeLegalMoves(int row, int col, IPiece[,] boardPieces);

        public abstract bool CanPush();

    }
}
