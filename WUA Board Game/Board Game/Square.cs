﻿using System;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Board_Game
{
    internal class Square : IBoardSquare
    {
        public int Row { get; set; }
        public int Column { get; set; }

        public Square(int row, int column)
        {
            this.Row = row;
            this.Column = column;
        }

        public Square() : this(0, 0) { }

        public override bool Equals(object obj)
        {
            var square = obj as Square;

            if (square == null)
                return false;

            return (Row == square.Row) && (Column == square.Column);
        }

        protected bool Equals(Square other)
        {
            return Row == other.Row && Column == other.Column;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Row*397) ^ Column;
            }
        }
    }
}
