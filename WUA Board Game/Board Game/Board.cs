﻿using System;
using System.Collections.Generic;
using WUA_Board_Game.Board_Game_Interfaces;
using WUA_Board_Game.Pusher_Game;

namespace WUA_Board_Game.Board_Game
{
    internal abstract class Board : IBoard
    {
        public const int BOARD_SIZE = 8;

        public IPlayer Player1 { get; set; }
        public IPlayer Player2 { get; set; }

        public IPiece[,] Pieces { get; set; }
        public TeamFlag CurrentPlayer { get; internal set; }

        public List<IPiece> WhitePieces { get; private set; }
        public List<IPiece> BlackPieces { get; private set; }

        private List<IObserver<IMove>> _observers;

        private AI whiteComputer;
        private AI blackComputer;

        protected Board()
        {
            
        }
        /// <summary>
        /// Moves the piece from its origin location to its target location.
        /// Will trigger recomputing of legal moves for affected Pieces
        /// Does not verify if the move done is legal!
        /// Will create an IMove from the provided move coordinates.
        /// Will alert observers of the move.
        /// </summary>
        /// <param name="oRow">The row of the Piece to move.</param>
        /// <param name="oCol">The column of the Piece to move.</param>
        /// <param name="tRow">The target row of the move.</param>
        /// <param name="tCol">The target column of the move.</param>
        /// <returns>An IMove object which contains information about the move done.</returns>
        public IMove MovePiece(int oRow, int oCol, int tRow, int tCol)
        {
            IMove move = new Move(oRow, oCol, tRow, tCol, Pieces[oRow, oCol], Pieces[tRow, tCol], CurrentPlayer);

            PlayMove(oRow, oCol, tRow, tCol);

            move.Victory = IsVictoryAchieved();

            SwitchTurn();
            NotifyObservers(move);

            PlayComputers();
            return move;
        }

        /// <summary>
        /// Execute the specified move.
        /// Does not create a new IMove for the return.
        /// Faster than MovePiece(int, int, int, int).
        /// Does not notify observers of the move.
        /// </summary>
        /// <param name="move"></param>
        /// <returns>The specified move</returns>
        public IMove MovePiece(IMove move)
        {
            PlayMove(move.Origin.Row, move.Origin.Column, move.Target.Row, move.Target.Column);

            move.Victory = IsVictoryAchieved();

            SwitchTurn();
            //PlayComputers();

            return move;
        }

        private void PlayComputers()
        {
            switch (CurrentPlayer)
            {
                case TeamFlag.White:
                    if (whiteComputer != null)
                    {
                        var bestMove = whiteComputer.GetBestMove();
                        MovePiece(bestMove.Origin.Row, bestMove.Origin.Column, bestMove.Target.Row,
                            bestMove.Target.Column);
                    }
                        

                    break;

                case TeamFlag.Black:
                    if (blackComputer != null)
                    {
                        var bestMove = blackComputer.GetBestMove();
                        MovePiece(bestMove.Origin.Row, bestMove.Origin.Column, bestMove.Target.Row,
                            bestMove.Target.Column);
                    }
                        
                    break;

                default:
                    break;
            }
        }

        private void PlayMove(int oRow, int oCol, int tRow, int tCol)
        {
            MovePieceLocation(oRow, oCol, tRow, tCol);
            RecomputeAffectedPieces(oRow, oCol, tRow, tCol);
        }

        private void MovePieceLocation(int oRow, int oCol, int tRow, int tCol)
        {
            RemovePiece(tRow, tCol);
            Pieces[tRow, tCol] = Pieces[oRow, oCol];
            Pieces[oRow, oCol] = null;
        }

        private void MovePieceLocation(IMove move)
        {
            MovePieceLocation(move.Origin.Row, move.Origin.Column, move.Target.Row, move.Target.Column);
        }

        internal abstract bool IsVictoryAchieved();

        public bool IsLegalMove(int oRow, int oCol, int tRow, int tCol)
        {
            return IsCurrentTeamPiece(Pieces[oRow, oCol]) && Pieces[oRow, oCol].IsLegalMove(tRow, tCol);
        }

        private bool IsCurrentTeamPiece(IPiece piece)
        {
            return (TeamFlag) piece.Owner.Team == CurrentPlayer;
        }

        public virtual void StartNewGame(PlayerType player1Type, PlayerType player2Type)
        {

            Player1 = new Player(TeamFlag.White, player1Type);
            Player2 = new Player(TeamFlag.Black, player2Type);

            Pieces = new IPiece[BOARD_SIZE, BOARD_SIZE];
            WhitePieces = new List<IPiece>(16);
            BlackPieces = new List<IPiece>(16);

            CurrentPlayer = TeamFlag.White;

            _observers = new List<IObserver<IMove>>(1);

            var gameAnalyzer = GetGameAnalyzer();

            whiteComputer = player1Type == PlayerType.Computer ? new AI(Player1.Team, Player2.Team, this, gameAnalyzer) : null;

            blackComputer = player2Type == PlayerType.Computer ? new AI(Player2.Team, Player1.Team, this, gameAnalyzer) : null;

            
        }

        internal abstract IGameAnalyzer GetGameAnalyzer();

        private void RecomputeAffectedPieces(int oRow, int oCol, int tRow, int tCol)
        {
            ComputeAllLegalMoves();
        }

        private void RecomputeAffectedPieces(IMove move)
        {
            RecomputeAffectedPieces(move.Origin.Row, move.Origin.Column, move.Target.Row, move.Target.Column);
        }

        internal void ComputeAllLegalMoves()
        {
            for (var i = 0; i < BOARD_SIZE; i++)
            {
                for (var j = 0; j < BOARD_SIZE; j++)
                {
                    ComputeLegalMove(i, j);
                }
            }
        }
  
        public void ComputeLegalMove(int row, int col)
        {
            Pieces[row, col]?.ComputeLegalMoves(row, col, Pieces);
        }

        public IBoardState CreateBoardState()
        {
            return new BoardState(Pieces);
        }

        public void RevertToState(IBoardState state)
        {
            Pieces = state.Pieces;
        }

        public void Undo(IMove move)
        {
            var tRow = move.Target.Row;
            var tCol = move.Target.Column;

            MovePieceLocation(tRow, tCol, move.Origin.Row, move.Origin.Column);
            PutPiece(tRow, tCol, move.TargetPiece);

            RecomputeAffectedPieces(move);
            SwitchTurn();
        }

        /// <summary>
        /// Will put a piece on the board.
        /// </summary>
        /// <param name="row">The row where to place the piece</param>
        /// <param name="col">The column where to place the piece</param>
        /// <param name="piece">The piece to add</param>
        internal void PutPiece(int row, int col, IPiece piece)
        {
            Pieces[row, col] = piece;

            if (piece != null)
            {
                switch ((TeamFlag) piece.Owner.Team)
                {
                    case TeamFlag.White:
                        WhitePieces.Add(piece);
                        break;

                    case TeamFlag.Black:
                        BlackPieces.Add(piece);
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Remove the piece at the specified location from the board.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        private void RemovePiece(int row, int col)
        {
            IPiece piece = Pieces[row, col];

            if (piece == null)
                return;

            //TODO: Maybe remove?
            Pieces[row, col] = null;

            switch ((TeamFlag) piece.Owner.Team)
            {
                case TeamFlag.White:
                    WhitePieces.Remove(piece);
                    break;

                case TeamFlag.Black:
                    BlackPieces.Remove(piece);
                    break;

                default:
                    break;
            }

        }

        public List<IMove> GetAllLegalMoves()
        {
            var legalMoves = new List<IMove>(40);

            foreach (var piece in WhitePieces)
            {
                legalMoves.AddRange(piece.LegalMoves);
            }

            foreach (var piece in BlackPieces)
            {
                legalMoves.AddRange(piece.LegalMoves);
            }

            return legalMoves;
        }

        /// <summary>
        /// Verify if the specified row and column coordinates are out of this Board bounds.
        /// </summary>
        /// <param name="row">The row coordinate.</param>
        /// <param name="col">The column coordinate.</param>
        /// <returns>True if is outside of the bounds of this board, false otherwise.</returns>
        public static bool IsOutOfBound(int row, int col)
        {
            return !(row >= 0 && col >= 0 && row < BOARD_SIZE && col < BOARD_SIZE);
        }

        private void SwitchTurn()
        {
            switch (CurrentPlayer)
            {
                case TeamFlag.White:
                    CurrentPlayer = TeamFlag.Black;
                    break;
              
                case TeamFlag.Black:
                    CurrentPlayer = TeamFlag.White;
                    break;

                default:
                    break;
            }
        }

        public IDisposable Subscribe(IObserver<IMove> observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);
            return new Unsubscriber(_observers, observer);
        }

        private void NotifyObservers(IMove move)
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(move);
            }
        }

        private class Unsubscriber : IDisposable
        {
            private readonly List<IObserver<IMove>> _observers;
            private readonly IObserver<IMove> _observer;

            public Unsubscriber(List<IObserver<IMove>> observers, IObserver<IMove> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }
    }
}
