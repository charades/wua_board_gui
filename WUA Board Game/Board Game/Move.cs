﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Board_Game
{
    internal class Move : IMove
    {
        public IBoardSquare Origin { get; set; }
        public IBoardSquare Target { get; set; }
        public IPiece MovedPiece { get; set; }
        public IPiece TargetPiece { get; set; }
        public TeamFlag Team { get; set; }
        public bool Victory { get; set; }

        public Move(int oRow, int oCol, int tRow, int tCol, IPiece movedPiece, IPiece targetPiece, TeamFlag team)
        {
            Origin = new Square(oRow, oCol);
            Target = new Square(tRow, tCol);
            MovedPiece = movedPiece;
            TargetPiece = targetPiece;
            Team = team;
        }
    }
}
