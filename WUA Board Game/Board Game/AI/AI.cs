﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls;
using WUA_Board_Game.Board_Game;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Pusher_Game
{
    
    internal class AI : IAI
    {
        private const int THINK_TIME_MILLIS = 5000;

        private TeamFlag maxPlayer;
        private TeamFlag minPlayer;
        private IBoard board;
        private IGameAnalyzer analyzer;

        private int thinkStartTime;
        private int shuffleDepth;

        private Stopwatch stopwatch;

        public AI(TeamFlag maxPlayer, TeamFlag minPlayer, IBoard board, IGameAnalyzer analyzer)
        {
            this.minPlayer = minPlayer;
            this.maxPlayer = maxPlayer;
            this.board = board;
            this.analyzer = analyzer;

            stopwatch = new Stopwatch();
            //TODO: Initialize strategy for max player.
        }

        /// <summary>
        /// Get the best move the computer can think of in under THINK_TIME_MILLIS milliseconds.
        /// </summary>
        /// <returns>An IMove object representing the best move found by the computer.</returns>
        public IMove GetBestMove()
        {
            stopwatch.Start();

            var boardSnapshot = board.CreateBoardState();
            object[] moveInfo = {null, null};
            var level = 1;

            do
            {
                shuffleDepth = level;

                try
                {
                    moveInfo = PrincipalVariationAlphaBeta(level++, maxPlayer, int.MinValue, int.MaxValue,
                        new List<IMove>(10));
                }
                catch (OutOfTimeException outOfTimeException)
                {
                    //When we run out of time for thinking, we throw an exception to stop execution right away.
                    //It leaves the board in a messed up state, so we restore it.
                    
                    break;
                }
                board.RevertToState(boardSnapshot);
                if ((int) moveInfo[0] > 9999 || (int) moveInfo[0] < -9999)
                    break;

                Debug.WriteLine(moveInfo[1] + ":" + moveInfo[0]);
                
            } while (stopwatch.ElapsedMilliseconds < THINK_TIME_MILLIS);

            return (IMove) moveInfo[1];
        }

        private object[] PrincipalVariationAlphaBeta(int depth, TeamFlag currentPlayer, int alpha, int beta, List<IMove> currentLine)
        {
            if (stopwatch.ElapsedMilliseconds >= THINK_TIME_MILLIS)
                throw new OutOfTimeException("Time's up!");

            
            int score;
            IMove bestMove = null;

            if (depth == 0)
            {
                score = analyzer.Analyze(board, this.maxPlayer);
                return new object[] { score, null };
            }

            //First try the previous best branch
            if (currentLine.Any())
            {
                IMove principalVariationMove = currentLine.First();
                currentLine.RemoveAt(0);

                board.MovePiece(principalVariationMove);

                if (currentPlayer == maxPlayer)
                {
                    score = (int) PrincipalVariationAlphaBeta(depth - 1, minPlayer, alpha, beta, currentLine)[0];

                    if (score > alpha)
                    {
                        alpha = score;
                        bestMove = principalVariationMove;
                    }

                }
                else
                {
                    score = (int) PrincipalVariationAlphaBeta(depth - 1, maxPlayer, alpha, beta, currentLine)[0];

                    if (score < beta)
                    {
                        beta = score;
                        bestMove = principalVariationMove;
                    }

                }

                board.Undo(principalVariationMove);
            }

            //Then we check all other branches.
            var moveLine = new List<IMove>(10);
            
            foreach (var move in board.GetAllLegalMoves())
            {
                board.MovePiece(move);

                if (currentPlayer == maxPlayer)
                {
                    score = (int) PrincipalVariationAlphaBeta(depth - 1, minPlayer, alpha, beta, moveLine)[0];

                    if (score > alpha)
                    {
                        alpha = score;
                        bestMove = move;
                    }

                }
                else
                {
                    score = (int) PrincipalVariationAlphaBeta(depth - 1, maxPlayer, alpha, beta, moveLine)[0];

                    if (score < beta)
                    {
                        beta = score;
                        bestMove = move;
                    }

                }

                board.Undo(move);

                if (alpha >= beta)
                {
                    currentLine.Add(move);
                    currentLine.AddRange(moveLine);
                    break;
                }
            }

       

            return new object[]
            {
                currentPlayer == maxPlayer ? alpha : beta,
                bestMove
            };
        }

    }
}
