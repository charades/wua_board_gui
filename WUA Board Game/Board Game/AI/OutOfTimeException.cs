﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WUA_Board_Game.Board_Game
{
    internal class OutOfTimeException : Exception
    {
        public OutOfTimeException()
        {
            
        }

        public OutOfTimeException(string message) : base(message)
        {
        }

        public OutOfTimeException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
