﻿using System;
using System.Collections.Generic;
using WUA_Board_Game.Board_Game;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Pusher_Game
{
    internal class Pushee : Piece
    {
        private const int MaxPossibleMoves = 3;
        public Pushee(IPlayer owner)
        {
            this.Owner = owner;
            this.InitImage(new Uri("ms-appx:///Assets/chess-pawn-icon-white-pushee.png", UriKind.Absolute),
                new Uri("ms-appx:///Assets/chess-pawn-icon-black-pushee.png", UriKind.Absolute));
            LegalMoves = new List<IMove>(MaxPossibleMoves);
        }

        public override int Score => 3;

        public override void ComputeLegalMoves(int row, int col, IPiece[,] boardPieces)
        {
            LegalMoves = new List<IMove>(3);

            var targetRow = row + Owner.GetDirection();
            var pusherRow = row - Owner.GetDirection();
            var targetCol = col - 1;
            var pusherCol = col + 1;
            //Iterate through front-left, front and front-right directions.
            for ( ; targetCol <= col + 1; targetCol++)
            {
                if (!Board.IsOutOfBound(targetRow, targetCol) && !Board.IsOutOfBound(pusherRow, pusherCol) && (boardPieces[pusherRow, pusherCol] != null && boardPieces[pusherRow, pusherCol].CanPush()) && (boardPieces[targetRow, targetCol] == null || !boardPieces[targetRow, targetCol].Owner.Equals(this.Owner)))
                    LegalMoves.Add(new Move(row, col, targetRow, targetCol, this, boardPieces[targetRow, targetCol], (TeamFlag) Owner.Team));
                pusherCol--;
            }
        }

        public override bool CanPush()
        {
            return false;
        }
    }
}
