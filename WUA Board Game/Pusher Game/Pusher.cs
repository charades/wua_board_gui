﻿using System;
using System.Collections.Generic;
using WUA_Board_Game.Board_Game;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Pusher_Game
{
    internal class Pusher : Piece
    {

        private const int MAX_POSSIBLE_MOVES = 3;

        public override int Score => 7;

        public Pusher(IPlayer owner)
        {
            this.Owner = owner;
            this.InitImage(new Uri("ms-appx:///Assets/chess-pawn-icon-white.png", UriKind.Absolute),
                new Uri("ms-appx:///Assets/chess-pawn-icon-black.png", UriKind.Absolute));
            LegalMoves = new List<IMove>(MAX_POSSIBLE_MOVES);
        }

        

        public override void ComputeLegalMoves(int row, int col, IPiece[,] boardPieces)
        {
            LegalMoves = new List<IMove>(3);
            var targetRow = row + Owner.GetDirection();

            //Iterate through front-left, front and front-right directions.
            for (var targetCol = col - 1; targetCol <= col + 1; targetCol++) {
                if (!Board.IsOutOfBound(targetRow, targetCol) && (boardPieces[targetRow, targetCol] == null || boardPieces[targetRow, targetCol].Owner != this.Owner))
                    LegalMoves.Add(new Move(row, col, targetRow, targetCol, this, boardPieces[targetRow, targetCol], (TeamFlag) Owner.Team));
            }

        }

        public override bool CanPush()
        {
            return true;
        }
    }
}
