﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Pusher_Game
{
    internal class PusherGameAnalyzer : IGameAnalyzer
    {
        public int Analyze(IBoard board, TeamFlag maxPlayer)
        {

            switch (maxPlayer)
            {
                case TeamFlag.White:
                    return AnalyzeWhiteStrategy(board);

                case TeamFlag.Black:
                    return AnalyzeBlackStrategy(board);

                    //TODO: Throw exception instead...
                default:
                    return 0;

            }

        }

        private static int AnalyzeWhiteStrategy(IBoard board)
        {
            return board.WhitePieces.Sum(piece => piece.Score);
        }

        private static int AnalyzeBlackStrategy(IBoard board)
        {
            return board.BlackPieces.Sum(piece => piece.Score);
        }
    }
}
