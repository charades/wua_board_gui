﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WUA_Board_Game.Board_Game;
using WUA_Board_Game.Board_Game_Interfaces;

namespace WUA_Board_Game.Pusher_Game
{
    internal class PusherBoard : Board
    {

        public PusherBoard() : base()
        {
           
        }
        internal override bool IsVictoryAchieved()
        {
            switch (CurrentPlayer)
            {
                //If white has reached the other side of the board, he has won.
                case TeamFlag.White:
                    for (var j = 0; j < BOARD_SIZE; j++)
                    {
                        if (Pieces[BOARD_SIZE - 1, j]?.Owner.Team == TeamFlag.White)
                            return true;
                    }
                    break;

                //If black has reached the other side of the board, he has won.
                case TeamFlag.Black:
                    for (var j = 0; j < BOARD_SIZE; j++)
                    {
                        if (Pieces[0, j]?.Owner.Team == TeamFlag.Black)
                            return true;
                    }
                    break;

                default:
                    break;
            }

            return false;
        }

        public override void StartNewGame(PlayerType player1Type, PlayerType player2Type)
        {
            base.StartNewGame(player1Type, player2Type);

            for (var j = 0; j < BOARD_SIZE; j++)
            {
                PutPiece(0, j, new Pusher(Player1));
                PutPiece(1, j, new Pushee(Player1));
                PutPiece(6, j, new Pushee(Player2));
                PutPiece(7, j, new Pusher(Player2));
            }

            ComputeAllLegalMoves();
        }

        internal override IGameAnalyzer GetGameAnalyzer()
        {
            return new PusherGameAnalyzer();
        }
    }
}
