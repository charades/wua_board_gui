﻿namespace WUA_Board_Game.Board_Game_Interfaces
{
    /// <summary>
    /// Records a move information.
    /// </summary>
    internal interface IMove
    {
        IBoardSquare Origin { get; set; }
        IBoardSquare Target { get; set; }
        IPiece MovedPiece { get; set; }
        IPiece TargetPiece { get; set; }
        TeamFlag Team { get; set; }
        bool Victory { get; set; }
    }
    
}
