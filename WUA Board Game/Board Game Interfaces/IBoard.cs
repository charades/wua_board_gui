﻿using System;
using System.Collections.Generic;

namespace WUA_Board_Game.Board_Game_Interfaces
{
    internal interface IBoard : IObservable<IMove>
    {
        IPiece[,] Pieces { get; set; }
        TeamFlag CurrentPlayer { get; }

        List<IPiece> WhitePieces { get; }
        List<IPiece> BlackPieces { get; }
    

            /// <summary>
        /// Will try to move a piece from its origin location to its target location.
        /// </summary>
        /// <param name="oRow">The origin row of the piece to move.</param>
        /// <param name="oCol">The origin column of the piece to move.</param>
        /// <param name="tRow">The target row of the piece to move.</param>
        /// <param name="tCol">The target column of the piece to move.</param>
        /// <returns>
        /// An IMove object containing information about the move that was tried. 
        /// </returns>
        IMove MovePiece(int oRow, int oCol, int tRow, int tCol);

        IMove MovePiece(IMove move);

        /// <summary>
        /// Query the board to validate a move.
        /// </summary>
        /// <param name="oRow">The origin row of the piece to move.</param>
        /// <param name="oCol">The origin column of the piece to move.</param>
        /// <param name="tRow">The target row of the piece to move.</param>
        /// <param name="tCol">The target column of the piece to move.</param>
        /// <returns>True if the move is valid, false otherwise.</returns>
        bool IsLegalMove(int oRow, int oCol, int tRow, int tCol);

        /// <summary>
        /// Clean the board and initialize it for a new game.
        /// </summary>
        /// <param name="player1Type">The PlayerType of the first player.</param>
        /// <param name="player2Type">The PlayerType of the second player</param>
        void StartNewGame(PlayerType player1Type, PlayerType player2Type);

        /// <summary>
        /// Compute the legal moves for the piece at the specified coordinates.
        /// </summary>
        /// <param name="row">The row coordinate of the piece.</param>
        /// <param name="col">The column coordinate of the piece.</param>
        void ComputeLegalMove(int row, int col);

        /// <summary>
        /// Creates a memento of the current IBoard state.
        /// </summary>
        /// <returns>An IBoardState representing the state of the board at the moment
        /// it was created.</returns>
        IBoardState CreateBoardState();

        /// <summary>
        /// Put the current IBoard in the same state as the specified IBoardState.
        /// </summary>
        /// <param name="state">The state to mirror with the current IBoard</param>
        void RevertToState(IBoardState state);

        /// <summary>
        /// Execute the move in reverse.
        /// Revives any casualties.
        /// </summary>
        /// <param name="move">The move to undo</param>
        void Undo(IMove move);

        /// <summary>
        /// Must return all the legal moves of the current turn player pieces.
        /// </summary>
        /// <returns>All moves that can be played this turn.</returns>
        List<IMove> GetAllLegalMoves();
    }

}
