﻿using System.Collections.Generic;
using Windows.UI.Xaml.Controls;

namespace WUA_Board_Game.Board_Game_Interfaces
{
    internal interface IPiece
    {
        List<IMove> LegalMoves { get; set; }
        IPlayer Owner { get; set; }
        Image Image { get; set; }

        int Score { get; }

        /// <summary>
        /// Move the Piece to the specified row and column coordinates
        /// </summary>
        /// <param name="row">The row to move the piece to.</param>
        /// <param name="col">The column to move the piece to.</param>
        /// <returns>True if the move was successful/valid. False otherwise.</returns>
        bool Move(int row, int col);

        /// <summary>
        /// Verify if the specified IBoardSquare is a legal target move for this piece.
        /// </summary>
        /// <param name="tRow">The target row of the move.</param>
        /// <param name="tCol">The target column of the move.</param>
        /// <returns>True if legal, false otherwise.</returns>
        bool IsLegalMove(int tRow, int tCol);

        /// <summary>
        /// Compute the LegalTargetSquare for this IPiece.
        /// </summary>
        /// <param name="row">The current row position of the piece.</param>
        /// <param name="col">The current column position of the piece.</param>
        /// <param name="boardPieces">The current position of all the pieces on the board.</param>
        void ComputeLegalMoves(int row, int col, IPiece[,] boardPieces);

        bool CanPush();
    }
}
