﻿namespace WUA_Board_Game.Board_Game_Interfaces
{
    internal interface IBoardSquare
    {
        int Row { get; set; }
        int Column { get; set; }
    }

}
