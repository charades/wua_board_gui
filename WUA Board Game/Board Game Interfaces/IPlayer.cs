﻿using Windows.Media.Playback;

namespace WUA_Board_Game.Board_Game_Interfaces
{
    public interface IPlayer
    {
        TeamFlag Team { get; set; }
        PlayerType PlayerType { get; set; }

        int GetDirection();
    }
}