﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WUA_Board_Game.Board_Game_Interfaces
{
    /// <summary>
    /// Used to identify if a player is a local Human, a local Computer, or playing over the network.
    /// </summary>
    public enum PlayerType
    {
        Human,
        Computer,
        Network
    }
}
