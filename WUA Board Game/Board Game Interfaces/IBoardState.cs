﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WUA_Board_Game.Board_Game_Interfaces
{
    internal interface IBoardState
    {
        IPiece[,] Pieces { get; }

    }
}
